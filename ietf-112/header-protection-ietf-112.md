# LAMPS E-mail Header Protection

## IETF 112 OpenPGP (Nov 2021)

Daniel Kahn Gillmor <dkg@fifthhorseman.net>

[draft-ietf-lamps-header-protection](https://datatracker.ietf.org/doc/draft-ietf-lamps-header-protection/)

---

# Stalled

Multiple options in the draft.

Unclear how to choose.

Set of criteria proposed, little to no feedback.

---

# Do we need to ask different questions?

All proposals would work for new clients.

Sticking point seems to be handling by legacy clients.

---

# Proposal for evaluation

Minimize any change in experience for legacy clients.

Ignore security increase for legacy clients.

---

# Rationale

Incentivize moving to compliant clients

Break logjam

---

# Questions? Suggestions?
