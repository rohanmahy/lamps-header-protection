Screenshots for Evolution
=========================

These screenshots are captured directly from the git repository, with no external server.

They're generated by running `screenshots/evolution/tools/screenshot-all` from the root directory of the repository on a GNU/Linux system.

On debian, to ensure that they work, you need to first make sure all the dependencies are installed: 

    # apt install evolution xdotool xvfb x11-apps graphicsmagick x11-utils

Notes
-----

This scripting uses local mail folder mechanism in Evolution, and avoids any network access.

To work, it needs to take over the user's evolution installation (e.g. in `~/.config/evolution`, `~/.cache/evolution`, `~/.local/share/evolution`) and stock NSS database (`~/.pki/nssdb`).
It's probably safest to run it from a dedicated account.
