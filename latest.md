---
title: Editor's copy of "Header Protection for S/MIME"
---

You can see here the Editor's copy of the latest revision of "Header Protection for S/MIME":

- [HTML](draft-ietf-lamps-header-protection.html)
- [TXT](draft-ietf-lamps-header-protection.txt)

The most recent formally published version can be found [at the IETF datatracker](https://datatracker.ietf.org/doc/draft-ietf-lamps-header-protection/).
Look at gitlab for [revision control](https://gitlab.com/dkg/lamps-header-protection) and [an issue tracker](https://gitlab.com/dkg/lamps-header-protection/-/issues).

Discussion for this document happens on [the LAMPS mailing list](https://www.ietf.org/mailman/listinfo/spasm).
