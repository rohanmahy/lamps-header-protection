#!/bin/bash

#set -x
set -e
set -o pipefail
# cmd is either "both" (meaning, show both render and reply) or "wait"

cmd="$1"
msg="$2"
version="$3"
msgname="$(basename "$msg" .eml)"

if [ -z "$version" ]; then
    version="$(evolution --version | awk '{ print $2 }')"
    mkdir -p "screenshots/evolution/$version/"{render,reply}
fi

for x in ~/.config/evolution ~/.local/share/evolution ~/.cache/evolution ~/.pki/nssdb; do
    if [ -e "$x" ]; then
        printf "%s already exists, bailing\n" "$x" >&2
        exit 1
    fi
done

window_by_title() {
    titlematch="$1"
    action="$2"
    fname="$3"
    while sleep 0.2; do
#        xwininfo -root -children
        id=$(xwininfo -root -children | awk /"$titlematch"/'{ print $1 }')
        if [ -n "$id" ]; then
            break
        fi
    done
    sleep 0.5 # hack: wait to ensure rendering is complete
    printf "%s: %s\n" "$msgname" "$action" >&2

    if [ -n "$fname" ]; then
        xwd -id "$id" | gm convert - "$fname"
    fi
}

mkdir -p ~/.config/evolution/sources ~/.local/share/evolution/mail/local/{,.Outbox/}{cur,new,tmp} ~/.pki/nssdb

printf 'maildir++ 1' > ~/.local/share/evolution/mail/local/..maildir++
cp screenshots/evolution/tools/cmeta  ~/.local/share/evolution/mail/local/..cmeta
cp screenshots/evolution/tools/cmeta  ~/.local/share/evolution/mail/local/.Outbox.cmeta

profdir="$(realpath ~/.pki/nssdb)"
certutil -A -d "sql:$profdir" -n 'LAMPS Sample CA' -t ,TC, -a -i ca.rsa.crt
pk12util -i bob.pfx -W 'bob' -d "sql:$profdir"

cat > ~/.config/evolution/sources/1111111111111111111111111111111111111111.source <<EOF
[Data Source]
DisplayName=bob@smime.example
Enabled=true
Parent=

[Mail Account]
BackendName=none
IdentityUid=4444444444444444444444444444444444444444
NeedsInitialSetup=false

[Refresh]
Enabled=false
EOF

cat > ~/.config/evolution/sources/2222222222222222222222222222222222222222.source <<EOF
[Data Source]
DisplayName=bob@smime.example
Enabled=true
Parent=1111111111111111111111111111111111111111
[Mail Transport]
BackendName=sendmail
[Sendmail Backend]
EOF

cat > ~/.config/evolution/sources/3333333333333333333333333333333333333333.source <<EOF
[Data Source]
DisplayName=bob@smime.example
Enabled=true
Parent=1111111111111111111111111111111111111111

[Mail Submission]
SentFolder=folder://local/Sent
TransportUid=4444444444444444444444444444444444444444

[Mail Identity]
Address=bob@smime.example
Name=Bob
EOF

cp "$msg" ~/.local/share/evolution/mail/local/cur/


sqlite3 ~/.local/share/evolution/mail/local/folders.db <<EOF
BEGIN TRANSACTION;
CREATE TABLE folders ( folder_name TEXT PRIMARY KEY, version REAL, flags INTEGER, nextuid INTEGER, time NUMERIC, saved_count INTEGER, unread_count INTEGER, deleted_count INTEGER, junk_count INTEGER, visible_count INTEGER, jnd_count INTEGER, bdata TEXT );
INSERT INTO folders VALUES('Inbox',13.999999999999999999,0,0,0,1,1,0,0,1,0,'1');
CREATE TABLE IF NOT EXISTS 'Inbox' ( uid TEXT PRIMARY KEY , flags INTEGER , msg_type INTEGER , read INTEGER , deleted INTEGER , replied INTEGER , important INTEGER , junk INTEGER , attachment INTEGER , dirty INTEGER , size INTEGER , dsent NUMERIC , dreceived NUMERIC , subject TEXT , mail_from TEXT , mail_to TEXT , mail_cc TEXT , mlist TEXT , followup_flag TEXT , followup_completed_on TEXT , followup_due_by TEXT , part TEXT , labels TEXT , usertags TEXT , cinfo TEXT , bdata TEXT, created TEXT, modified TEXT);
CREATE TABLE IF NOT EXISTS 'Inbox_version' ( version TEXT );
INSERT INTO Inbox_version VALUES('2');
COMMIT;
EOF
## INSERT INTO Inbox VALUES('$msgname.eml',196608,0,0,0,0,0,0,0,1,7628,1613833802,0,'[...]','Alice <alice@smime.example>','Bob <bob@smime.example>',NULL,NULL,NULL,NULL,NULL,'109686927 1090030882 0','','0',NULL,'','1619620039','1619620039');

#strace -f -o evolution.strace
evolution --force-online --component mail &


window_by_title 'Mail' loading

xdotool mousemove 100 100 click 1 sleep 0.2 key --delay 200 Up Up Up Down Up Down sleep 0.2 mousemove 350 80 click 1 sleep 0.2 key Return

window_by_title 960x766 render
sleep 1
xwd -id "$id" | gm convert - "screenshots/evolution/$version/render/$msgname.png"

## FIXME: need to handle reply as well

xdotool key ctrl+r

window_by_title Re: reply "screenshots/evolution/$version/reply/$msgname.png"

evolution --quit
wait
