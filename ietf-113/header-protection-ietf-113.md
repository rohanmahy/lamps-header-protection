# LAMPS E-mail Header Protection

## IETF 113 OpenPGP (March 2022)

- Daniel Kahn Gillmor <dkg@fifthhorseman.net>
- Bernie Hoeneisen <bernie.hoeneisen@pep.foundation>
- Alexey Melnikov <alexey.melnikov@isode.com>

[draft-ietf-lamps-header-protection](https://datatracker.ietf.org/doc/draft-ietf-lamps-header-protection/)

---

# Recap

Two schemes:

## Wrapped Message

From S/MIME 3.1, not widely implemented, legacy MUAs treat it weirdly.

## Injected Headers

Obscured headers invisible for legacy MUAs, Legacy Display part tries to resolve that problem

But Legacy Display part causes problems in Outlook.

---

# Update: Legacy Display Evolved

Legacy Display MIME parts caused problems in Outlook.

Now they are Legacy Display Elements (inserted into main body parts).

No more problems with the legacy MUAs tested.

TODO: clearer picture as example

---

# Update: Choice of Scheme

Backward compatibility is important.

Conformant MUAs MUST be able to generate Injected Headers.

Conformant MUAs MAY generate Wrapped Message.

Conformant MUAs MUST be able to consume and render both schemes.

---

# More Help Needed

Test vectors (https://header-protection.cmrg.net/)
  - Test other MUAs 
  - Send sample messages through MTAs
  - Test automated systems

---

# Qs for the WG(1): Recommended HCP

- `hcp_minimal`
- `hcp_strong`

Design team leans toward recommending `hcp_minimal`

----

# Qs for the WG(2): Signalling for Legacy Display?

https://gitlab.com/dkg/lamps-header-protection/-/issues/20

When composing an encrypted message with injected headers, how does a MUA infer the need for Legacy Display?

- General ecosystem survey?

- Signalling from recipients that the recipient can render obscured injected headers?

Signalling is challenging

----

# Qs for the WG(3): Automated Mail systems

How are systems that use e-mail as a control channel affected by either scheme?

Do Legacy Display Elements interfere with processing?

- Mailing lists

- Bug trackers

- DNS control

----

# Qs for the WG(4): Legacy Display in `text/html`

```
<html><head><title></title></head><body>
<div class="header-protection-legacy-display">
<pre>Subject: Dinner plans</pre></div>
<p>Let's meet at Rama's Roti Shop at 8pm
and go to the park from there.
</p></body></html>
```

---

# Questions? Suggestions?
